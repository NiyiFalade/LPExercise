## Summary

The projects uses [Serenity BDD](http://www.thucydides.info/#/) utilising the [Screenplay pattern](http://serenity-bdd.info/docs/articles/screenplay-tutorial.html)

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.


## Other technologies Used on the project

Lombok ( Mapping responses to a Java Object)
Hamcrest ( Assertions)
Cucumber ( Test Scenarios using BDD format)


## Project directory Structure


![alt text](Screenshot from 2022-06-13 11-00-15.png)




# Running Locally and Installing needed dependencies 

Run this command from the root of the project

` mvn clean verify -Dcucumber.options="--tags @test"
`

## Reports

Once tests are completed the report can be found at:

`./target/site/serenity/index.html`



##Notes

# . Usages of Exceptions   "src/main/java/starter/exceptions"
#. Java Objects to map the get call valid and error response  ( Using lombok)    "src/main/java/starter/responses"
#. Using Serenity SessionVariable to store testdata during execution  "src/main/java/starter/utilities
#.  Moved the Get Api away from Step definitions to " src/test/java/endpoints" Using Serenity Screenplay Tasks
#.  added gitlab-ci-yml to execute a gitlab CI pipeline 


## gitlab ci/cd pipeline Test Report 

https://niyifalade.gitlab.io/-/LPExercise/-/jobs/2581525569/artifacts/target/site/serenity/index.html
