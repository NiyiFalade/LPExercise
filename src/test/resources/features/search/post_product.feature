@test
Feature: Search for the product
  As a User
  I want to be to search for available products


  Scenario Outline: Search for valid product
    Given David wants find a product
    When he calls endpoint <product>
    And  endpoint returns a status code <responseCode>
    Then he sees the results displayed for <product>

    Examples:
    |product|responseCode|
    |apple  |     200     |
    |mango  |     200     |


  Scenario Outline: Search for invalid product
    Given David wants find a product
    When he calls endpoint <product>
    And  endpoint returns a status code <responseCode>
    Then he doesn not see the results

    Examples:
      |product|responseCode|
      |car  |     404     |





