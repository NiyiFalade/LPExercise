package starter.stepdefinitions;


import cucumber.api.java.Before;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.util.EnvironmentVariables;
import starter.exceptions.MissingDataException;
import starter.utilities.TestData;

import static starter.utilities.TestData.DataKeys.API_URL;


public class Hooks {

    @Steps
    private TestData testData;


    private EnvironmentVariables environmentVariables;

    @Before
    public void setTheStage() {

        testData.setData(API_URL, environmentVariables.optionalProperty("environments.default.restapi.baseurl")
                .orElseThrow(() -> new MissingDataException("Missing environment property called restapi.baseurl")));

    }
}
