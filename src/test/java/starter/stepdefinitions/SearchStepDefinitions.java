package starter.stepdefinitions;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Steps;
import starter.endpoints.GetCall;
import starter.responses.ErrorResponse;
import starter.responses.FruityResponse;
import starter.utilities.TestData;

import java.util.List;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class SearchStepDefinitions {

    private Actor user;
    private List<FruityResponse> fruityResponse;
    private ErrorResponse errorResponse;
    ObjectMapper mapper = new ObjectMapper();

    @Steps
    TestData testData;


    @Given("^(.*) wants find a product")
    public void thereIsACampaignToDelete(String actor) {
        user = Actor.named(actor).whoCan(CallAnApi.at(testData.getData(TestData.DataKeys.API_URL)));
    }

    @When("^he calls endpoint (.*)")
    public void heCallsEndpoint(String product) {
        user.attemptsTo(
                GetCall.withProductID(product)
        );
    }

    @Then("^endpoint returns a status code (\\d+)")
    public void validateResponseCode(int statusCode){
        user.should(
                seeThatResponse("status code is returned",
                        response -> response.log().all().statusCode(statusCode))
        );

    }

    @Then("he sees the results displayed for (.*)")
    public void heSeesTheResultsDisplayedForApple(String product) throws JsonProcessingException {

            fruityResponse = mapper.readValue(SerenityRest.lastResponse().jsonPath().prettyPrint(), new TypeReference<List<FruityResponse>>(){});

            fruityResponse.stream().forEach((p) -> {
                assertThat(p.getTitle()).containsIgnoringCase(product);
            });
    }

    @Then("he doesn not see the results")
    public void he_Doesn_Not_See_The_Results() throws JsonProcessingException {
        errorResponse = mapper.readValue(SerenityRest.lastResponse().jsonPath().prettyPrint(), ErrorResponse.class);

        assertThat(errorResponse.getDetail().getError()).isTrue();
    }
}
