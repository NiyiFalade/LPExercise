package starter.endpoints;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.interactions.Get;
import net.thucydides.core.annotations.Step;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GetCall implements Task {

    private final String product;

    public GetCall(String product){
        this.product = product;
    }

    public static GetCall withProductID(String product) {
        return instrumented(GetCall.class, product);
    }

    @Override
    @Step("{0} fetches the product with product #")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Get.resource("/{product}")
                        .with(request -> request.log().all().pathParam("product", product))
        );
    }

}
