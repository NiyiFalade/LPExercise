package starter.responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FruityResponse {

    private String provider;
    private String title;
    private String url;
    private String price;
    private String unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;

}
