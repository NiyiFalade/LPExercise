package starter.responses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorResponse {
    private Detail detail;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Detail{
        private Boolean error;

    }
}
