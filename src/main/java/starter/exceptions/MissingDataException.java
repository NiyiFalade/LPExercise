package starter.exceptions;

public class MissingDataException extends IllegalArgumentException {

    public MissingDataException(String message) {
        super(message);
    }
}
